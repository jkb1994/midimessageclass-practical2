//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "cmath"

class MidiMessage
{
public:
    MidiMessage() // Constuctor
    {
        number = 60;
    }
    ~MidiMessage() // Destructor
    {
        
    }
    void setNoteNumber (int value) // Mutator
    {
        number = value;
    }
    int getNoteNumber() const // Accessor
    {
        return number;
    }
    float getMidiNoteInHertz() const // Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
private:
    int number;
};

int main ()
{
    int intNum;
    std::cin >> intNum;
    
    
    MidiMessage note;
    note.setNoteNumber(intNum);
    
    std::cout << note.getNoteNumber() << "\n";
    
    std::cout << note.getMidiNoteInHertz() << "\n";
    
    
    note.getNoteNumber();
    note.getMidiNoteInHertz();
    
   return 0;
}

